# M

## Content

```
./M. L. Bush:
M. L. Bush - Servitutea in epoca moderna.pdf

./Magdalena Marculescu Cojocea:
Magdalena Marculescu Cojocea - Critica metafizicii la Kant si Heidegger.pdf

./Manual:
Manual de limbi clasice - Greaca.pdf
Manual de limbi clasice - Latina.pdf

./Marc Aureliu:
Marc Aureliu - Catre sine insusi.pdf

./Marie Dominique Popeland:
Marie Dominique Popeland - Elemente de logica.pdf

./Marin Turlea:
Marin Turlea - Filosofia matematicii (Editura Universitatii din Bucuresti, 2002).pdf
Marin Turlea - Filosofia matematicii.pdf
Marin Turlea - Sens si adevar.pdf

./Mario Bunge:
Mario Bunge - Stiinta si filosofie.pdf

./Marshall McLuhan:
Marshall McLuhan - Galaxia Gutenberg.pdf

./Marsilio Ficino & Platon:
Marsilio Ficino & Platon - Banchetul, Asupra iubirii.pdf

./Marta Petreu:
Marta Petreu - Jocurile manierismului logic.pdf

./Martha Kneale & William Kneale:
Martha Kneale & William Kneale - Dezvoltarea logicii, vol. 1.pdf
Martha Kneale & William Kneale - Dezvoltarea logicii, vol. 2.pdf

./Martin Heidegger:
Martin Heidegger - Conceptul de timp.pdf
Martin Heidegger - Despre miza gandirii.pdf
Martin Heidegger - Fiinta si timp.pdf
Martin Heidegger - Introducere in metafizica.pdf
Martin Heidegger - Ontologie. Hermeneutica facticitatii.pdf
Martin Heidegger - Problemele fundamentale ale fenomenologiei.pdf
Martin Heidegger - Prolegomene la istoria conceptului de timp.pdf
Martin Heidegger - Repere pe drumul gindirii.pdf

./Martin Hollis:
Martin Hollis - Introducere in filosofia stiintelor sociale.pdf

./Matei Georgescu:
Matei Georgescu - Ipostaze ale mortii intr-un timp al dorintei.pdf

./Maurizio Viroli:
Maurizio Viroli - Din dragoste de patrie.pdf

./Max Scheler:
Max Scheler - Omul resentimentului.pdf

./Max Weber:
Max Weber - Etica protestanta si spiritul capitalismului.pdf

./Michael Carrithers:
Michael Carrithers - Buddha (Maestrii spiritului).pdf

./Michael Devitt:
Michael Devitt - Limbaj si realitate.pdf

./Michael Dummett:
Michael Dummett - Originile filosofiei analitice.pdf

./Michael Walzer:
Michael Walzer - Despre tolerare.pdf

./Michel de Montaigne:
Michel de Montaigne - Eseuri, vol.II (primul lipseste).pdf

./Michel Foucault:
Michel Foucault - Ordinea discursului.pdf

./Michel Shermer:
Michel Shermer - De ce oamenii cred in bazaconii.pdf

./Michio Kaku:
Michio Kaku - Fizica imposibilului.pdf

./Mihail Radu Solcan:
Mihail Radu Solcan - Arta raului cel mai mic.pdf
Mihail Radu Solcan - Conceptul de mecanism economic si diversele sisteme economice.pdf
Mihail Radu Solcan - Dezbateri pe marginea problemei corp-minte.pdf
Mihail Radu Solcan - Eseul filosofic.pdf
Mihail Radu Solcan - Freedom, Minds and Institutions.pdf
Mihail Radu Solcan - Introducere in filosofia mintii (ebook).pdf
Mihail Radu Solcan - Problema rationalitatii in gandirea filosofica si stiintifica.pdf

./Mihai Nasta:
Mihai Nasta - Despre Pitagora si pitagoricieni.pdf

./Mihai Novac:
Mihai Novac - Constiinta si fenomen (Conceptul de ego cogito intre kantianism si fenomenologia lui Husserl).pdf

./Mikel Dufrenne:
Mikel Dufrenne - Fenomenologia experientei estetice, vol. 1.pdf
Mikel Dufrenne - Fenomenologia experientei estetice, vol. 2.pdf

./Mircea Dumitru:
Mircea Dumitru - Modalitate si incompletitudine.pdf

./Mircea Eliade:
Mircea Eliade - Cosmologie si alchimie babiloniana.pdf
Mircea Eliade - Despre Eminescu si Hasdeu.pdf
Mircea Eliade - Faurari si alchimisti.pdf
Mircea Eliade - Imagini si simboluri.pdf
Mircea Eliade - Mefistofel si androginul.pdf
Mircea Eliade - Nasteri mistice.pdf
Mircea Eliade - Ocultism, vrajitorie si mode culturale.pdf
Mircea Eliade - Samanismul si tehnicile arhaice ale extazului.pdf
Mircea Eliade - Tratat de istorie a religiilor.pdf

./Mircea Flonta:
Mircea Flonta - 20 de intrebari si raspunsuri despre Immanuel Kant.pdf
Mircea Flonta - Cognitio, o introducere in problema cunoasterii.pdf
Mircea Flonta - Cum recunoastem pasarea Minervei.pdf
Mircea Flonta - Darwin si gandirea evolutionista.pdf
Mircea Flonta - Descartes-Leibniz. Ascensiunea si posteritatea rationalismului clasic.pdf
Mircea Flonta - Filosoful-rege.pdf
Mircea Flonta - Ganditorul singuratic. Critica si practica filozofiei la Ludwig Wittgenstein.pdf
Mircea Flonta - Imagini ale stiintei.pdf
Mircea Flonta - Kant in lumea lui si in cea de azi.pdf
Mircea Flonta - Ludwig Wittgenstein in filosofia secolului XX.pdf
Mircea Flonta - Perspectiva filosofica si ratiune stiintifica.pdf

./Mircea Florian:
Mircea Florian - Cosmologia elena.pdf

./Mircea Itu:
Mircea Itu - Filosofia si istoria religiilor.pdf

./Montesquieu:
Montesquieu - Despre spiritul legilor, vol. 1.pdf
Montesquieu - Despre spiritul legilor, vol. 2.pdf
Montesquieu - Despre spiritul legilor, vol. 3.pdf

./Moritz Schlick:
Moritz Schlick - Forma si continut.pdf
```

